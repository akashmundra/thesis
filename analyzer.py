from cmath import inf
import json
from statistics import mean

import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
from visualizer import my_plotter
from heuristics_generator import heuristics

filename = "motor_bike_small"
path = 'data/' + filename+'/'
f = open(path + filename + '.json')
data = json.load(f)


data_transform = {}
min = inf
node = 'ibp04'

# find smallest list of data points to help modify dataset
for metricType, metrics in data.items():
    key = 'system'
    if metricType == 'io':
        key = 'sda'
    for metric, value in metrics.items():
        if len(value[key][node]['data']) < min:
            min = len(value[key][node]['data'])

# create dataset
for metricType, metrics in data.items():
    key = 'system'
    if metricType == 'io':
        key = 'sda'
    for metric, value in metrics.items():
        new_key = metricType + '.' + metric
        data_transform[new_key] = value[key][node]['data'][0:min]

threshold = 130000

print(data_transform.keys())
# visualizer test
t = []
for index, value in enumerate(data_transform['mem.Memory bandwidth']):
    if value > threshold:
        t.append(index)

split = []
if len(t) > 0:
    start = t[0]
    counter = start
    end = start
    for index, time in enumerate(t):
        if index < len(t) - 1:
            if counter == t[index + 1] - 1:
                counter = t[index + 1]
            else:
                end = counter
                if start == end:
                    start = t[index + 1]
                    counter = start
                    end = start
                    continue
                else:
                    split.append([start, end])
                    start = t[index + 1]
                    counter = start
                    end = start
        else:
            if counter == t[index] - 1:
                counter = t[index]
            else:
                end = counter
                if start == end:
                    continue
                else:
                    split.append([start, end])
x = range(min)
revised_split = []
print(len(split))
# for index in range(len(split)):
#     if split[index][0] < 101:
#         revised_split.append(split[index])
    
# split = revised_split
# print(split)

# print(split)
my_plotter(
    x[1:-1], 
    data_transform['mem.Memory bandwidth'][1:-1], 
    split, 
    {}, 
    'timestep (s)', 
    'memory bandwidth (MB/s)', 
    threshold,
    'Memory Bandwidth - ' + filename,
    'memory_bandwidth_' + filename + '.png',
    path
)

# print(len(data_transform['mem.Memory bandwidth']))
# code generator test
# code = heuristics()

# for i in range(min):
#     ldict = { 'data': data_transform, i: i }
#     exec(code,globals(),ldict)

# read/write bandwidth
memory_bandwidth = data_transform['mem.Memory bandwidth']
read_bandwidth = data_transform['mem.Memory Read Bandwidth']
write_bandwidth = data_transform['mem.Memory Write Bandwidth']

print(max(memory_bandwidth))

# mem_band_avg = mean(memory_bandwidth)
# print(mem_band_avg)
# read_band_avg = mean(read_bandwidth)
# print(read_band_avg)
# write_band_avg = mean(write_bandwidth)
# print(write_band_avg)
# Plotting both the curves in one graph
plt.plot(x[1:-1], memory_bandwidth[1:-1], label='memory')
plt.plot(x[1:-1], read_bandwidth[1:-1], color='r', label='read')
plt.plot(x[1:-1], write_bandwidth[1:-1], color='g', label='write')
# for piece in split:
#     plt.axvspan(piece[0], piece[1], color='red', alpha=0.5)
plt.title('Read and Write bandwidth MB/s - ' + filename)
plt.legend(loc="upper left")
plt.savefig(path + 'read_write_bandwidth_' + filename + '.png')
plt.show()

# Plotting 2 different graphs simultaneously
# figure, axis = plt.subplots(1, 2)

# axis[0].plot(x[1:-1], read_bandwidth[1:-1])
# axis[0].set_title("Read Bandwidth")
# for piece in split:
#     axis[0].axvspan(piece[0], piece[1], color='red', alpha=0.5)

# axis[1].plot(x[1:-1], write_bandwidth[1:-1])
# axis[1].set_title("Write Bandwidth")
# for piece in split:
#     axis[1].axvspan(piece[0], piece[1], color='red', alpha=0.5)
# plt.legend(loc='lower left')
# plt.show()

# cache level data
L2_bandwidth = data_transform['mem.L2 Bandwidth']
L2D_load_bandwidth = data_transform['mem.L2D Load Bandwidth']
L3_bandwidth = data_transform['mem.L3 Bandwidth']

plt.plot(x[1:-1], L2_bandwidth[1:-1], label='L2 bandwidth')
plt.plot(x[1:-1], L2D_load_bandwidth[1:-1], color='r', label='L2D load Bandwidth')
plt.plot(x[1:-1], L3_bandwidth[1:-1], color='g', label='L3 bandwidth')
# for piece in split:
#     plt.axvspan(piece[0], piece[1], color='red', alpha=0.5)
plt.title('Cache Level Bandwidth Values - ' + filename)
plt.xlabel('timestep (s)')
plt.ylabel('Cache Bandwidth MB/s')
plt.legend(loc="upper left")
plt.savefig(path +'cache_bandwidth_' + filename + '.png')
plt.show()

# Load to store ratio
loads_to_store = data_transform['cpu.Load to Store Ratio']

plt.plot(x[1:-1], loads_to_store[1:-1], label='memory')
# plt.plot(x[1:-1], loads_to_store[1:-1], color='r', label='L2S ratio')
# plt.plot(x[1:-1], write_bandwidth[1:-1], color='g', label='write')

# for piece in split:
#     plt.axvspan(piece[0], piece[1], color='red', alpha=0.5)
plt.title('Loads to store ratio - ' + filename)
plt.xlabel('timestep (s)')
plt.ylabel('Loads to Store Ratio')
plt.savefig(path + 'loads_to_store_' + filename + '.png')
plt.show()

# FLOPS SP/DP
flops_sp = data_transform['cpu.FLOPS SP']
flops_dp = data_transform['cpu.FLOPS DP']

plt.plot(x[1:-1], flops_dp[1:-1], label='Flops dp')
plt.plot(x[1:-1], flops_sp[1:-1], color='g', label='Flops sp')
# for piece in split:
#     plt.axvspan(piece[0], piece[1], color='red', alpha=0.5)
plt.title('FLOPS SP & DP - ' + filename)
plt.xlabel('timestep (s)')
plt.ylabel('FLOPS/s')
plt.legend(loc="upper left")
plt.savefig(path +'flops_' + filename + '.png')
plt.show()

# L3 Miss rate Request rate
l3_miss_rate = data_transform['mem.L3 Miss Rate']
l3_request_rate = data_transform['mem.L3 Request Rate']

plt.plot(x[1:-1], l3_miss_rate[1:-1], label='Miss rate')
plt.plot(x[1:-1], l3_request_rate[1:-1], color='g', label='Request rate')
# for piece in split:
#     plt.axvspan(piece[0], piece[1], color='red', alpha=0.5)
plt.title('L3 Miss and Request rate - ' + filename)
plt.xlabel('timestep (s)')
plt.ylabel('Rate')
plt.legend(loc="upper left")
plt.savefig(path +'l3_miss_request_rate_' + filename + '.png')
plt.show()

# CPI
cpi = data_transform['cpu.CPI']
plt.plot(x[1:-1], cpi[1:-1], label='CPI')
# for piece in split:
#     plt.axvspan(piece[0], piece[1], color='red', alpha=0.5)
plt.title('CPI - ' + filename)
plt.xlabel('timestep (s)')
plt.ylabel('CPI')
plt.legend(loc="upper left")
plt.savefig(path +'cpi_' + filename + '.png')
plt.show()


# mem_band_avg = mean(L2_bandwidth)
# print(mem_band_avg)
# read_band_avg = mean(L2D_load_bandwidth)
# print(read_band_avg)
# write_band_avg = mean(L3_bandwidth)
# print(write_band_avg)
# # Plotting both the curves in one graph
# plt.plot(x[1:-1], L2_bandwidth[1:-1], label='L2 Bandwidth')
# plt.plot(x[1:-1], L2D_load_bandwidth[1:-1], color='g', label='L2D Load Bandwidth')
# plt.plot(x[1:-1], L3_bandwidth[1:-1], color='k', label='L3 Bandwidth')
# for piece in split:
#     plt.axvspan(piece[0], piece[1], color='red', alpha=0.5)
# plt.title('Cache bandwidth values during high memory bandhwidth')
# plt.legend(loc='lower left')
# plt.show()





# Notes:
# 1. can data loads be replaced by memory read volume and/or cache level data volume?
#######

    # ans = ''
    # ldict = { data: data, ans: ans}
    # exec(code,globals(),ldict)
# for area in data:
#     internal_key = 'system'
#     if(area == 'io'):
#         internal_key = 'sda'
#     data['mem']['Load to Store Ratio'] = data['cpu']['Load to Store Ratio']
#     data['mem']['FLOPS DP'] = data['cpu']['FLOPS DP']
#     data['mem']['FLOPS SP'] = data['cpu']['FLOPS SP']
#     data['mem']['CPI'] = data['cpu']['CPI']
#     data['mem']['Clock'] = data['cpu']['Clock']

#     data['io']['Usage Iowait'] = data['cpu']['Usage Iowait']
#     dataframe[area] = {}
#     for metric in data[area]:
#         if(area == 'io' and metric == 'Usage Iowait'):
#             internal_key = 'system'
#         dataframe[area][metric] = {}
#         dataframe[area][metric] = data[area][metric][internal_key][node]['data']

# if len(sys.argv) > 1 and sys.argv[1] == 'io':
#     io_df = dataframe['io']
#     min = --1
#     for key in io_df:
#         if(min > len(io_df[key])):
#             min = len(io_df[key])
#     for key in io_df:
#         io_df[key] = io_df[key][0:min]
#     io = pd.DataFrame.from_dict(io_df)

    
# elif len(sys.argv) > 1 and sys.argv[1] == 'mem':
#     memory_df = dataframe['mem']
#     min = --1
#     for key in memory_df:
#         if(min > len(memory_df[key])):
#             min = len(memory_df[key])
#     for key in memory_df:
#         memory_df[key] = memory_df[key][0:min]
#     memory = pd.DataFrame.from_dict(memory_df)

#     metric_list = memory.columns.values.tolist()

#     for index, row in memory.iterrows():
#         # if memory bandwidth high - memory bound else compute bound
#         # DDR4 theoretical max bandwidth: 93,866.88 MB/s bandwidth
#         # https://www.intel.com/content/www/us/en/support/articles/000056722/processors/intel-core-processors.html
#         if(row['Memory bandwidth'] > 94000 * 0.7):
#         # memory bound analysis
#             # if loads to l3 miss ratio high - ok else - check for data locality
#             # try Load to store ratio.
#             # How to calculate "loads"?

#             # if loads to stores ratio high - check for data locality else - ok
#             if(row['Load to Store Ratio'] > 0.5):
#                 print('Check for data locality. timestep:', index*10)

#             # if inter-socket badhwidth high - check for data locality else - ok
#             # use QPI - only for some intel architectures (ask Nico)

#             # if single to double precision ratio high - ok else - use single precision
#             if(0 < row['FLOPS SP']/row['FLOPS DP'] < 0.5):
#                 print('Use single precision. timestep:', index*10)

#         else:
#             # print('Compute Bound analysis')
#             # if floating point operation rate high - ok else - **, ***
#             # FLOP per clock cycle per core for different architectures - https://en.wikipedia.org/wiki/FLOPS, https://en.wikichip.org/wiki/flops
#             # flop count / max system flops
#             # ask Nico how to understand the tables.
#             # FLOPS for cascadelake architectures - fp64 - 32 fp32 - 64
#             # number of cores = 2
#             # if((row['FLOPS DP'] + row['FLOPS SP']) > )
#                 # ** if ratio of vector and scalar operations high - ** else - Check data level parallelism
#                 # vectorization ratio
#                     # ** if AVX to SSE ratio high - in-depth analysis else - compile code with the AVX flag
#                     # check if important
#                     # *** if Single to Double precision ratio high - in-depth analysis else - use single precision 

#             # if branch misprediction to instructions ratio high - ** else - ok
#             # likwid counters for branch - https://github.com/RRZE-HPC/likwid/blob/master/groups/kabini/BRANCH.txt
#                 # ** if branch mispredicted to branches ratio high - reduce branch misprediction else - ok
            
#             # if rate of expensive instructions high - remove expensive instructions else - ok
#             # expensive instruction likwid counters - https://github.com/RRZE-HPC/likwid/wiki/PatternsHaswellEP
#             # high cpi could indicate expensive instructions

#             # if CPI low and FLOP rate high - ok elseif both low - check for object instatiation elseif CPI high - in-depth analysis
#             # very load dependent. Need to analyse for a whole run and find average max min values.

#             # if L3 Bandwidth high - in-depth analysis else - ok
#             # find good L3 bandwidth values
#             if(row['L3 Bandwidth'] > 8):
#                 print('In-depth analysis. timestep:', index*10)
            
#             instructions_rate = row['Clock'] / row['CPI'] # verfiy formula
#             # if L3 cache misses to instructions ratio high - ** else - ok
#             # print(row['L3 Miss Rate'] / instructions_rate)
#             if(row['L3 Miss Rate'] / instructions_rate > 0.5):
#                 # if L3 cache hits to misses ratio high - in-depth analysis else - check for data locality
#                 if(row['L3 Miss Ratio'] > 0.5):
#                     print('in-depth analysis')
#                 else:
#                     print('check for data locality')

#             # if Stall cycles rate high - Detailed analysis or consider SMT else - ok
#             # find a way around it with present metrics
#             # stall cycle likwid counters - https://github.com/RRZE-HPC/likwid/blob/master/groups/CLX/CYCLE_STALLS.txt

# elif len(sys.argv) > 1 and sys.argv[1] == 'load':

#     print('Load imbalance')

#     # inter node load imbalance high - inter-node load imbalance else - ok
#     # intra node load imbalance high - intra-node load imbalance else - ok

# elif len(sys.argv) > 1 and sys.argv[1] == 'other':
#     print('Analysis of usage of other resources')

#     # if parallel inefficiency (not manifested with small number of core) -- ignore for now

#     # if virtual memory usage stabilises - ok, increasing - Analyze code for memory leak, high - check memory usage else - ok

#     # if Network load imbalance no - ok else - Check for parallelization paradigm

#     # if Network bandwidth high - Check for parallelization paradigm else **
#     # if data is stored in shared storage, can cause high bandwidth
#         # ** if network bytes per packet ratio high - ok else - Consolidate network packets

#     # if Energy usage high - change the frequency settings to minimize energy else - ok
