module.exports = {
  preset: '@vue/cli-plugin-unit-jest/presets/typescript-and-babel',
  collectCoverage: process.env.NODE_ENV !== 'development',
  collectCoverageFrom: ['src/**/*.ts', 'src/**/*.vue'],
  setupFilesAfterEnv: ['./tests/setup.ts'],
  // TODO: Remove... this is a bug in jest (https://github.com/facebook/jest/issues/12036)
  transformIgnorePatterns: [
    '/node_modules/(?!d3|d3-array|internmap|delaunator|robust-predicates)',
  ],
}
