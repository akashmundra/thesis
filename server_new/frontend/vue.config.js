// eslint-disable-next-line @typescript-eslint/no-var-requires
// const webpack = require('webpack')
module.exports = {
  devServer: {
    host: 'localhost', // uncomment if you want the access outside of localhost
    https: true,
  },
  transpileDependencies: ['vuex-persist', 'vuetify', 'vue-echarts', 'resize-detector'],
  productionSourceMap: false,
  lintOnSave: false,
  chainWebpack: (config) => {
    config.optimization.minimizer('terser').tap((args) => {
      const _args = args
      _args[0].terserOptions.compress.drop_console = true
      _args[0].terserOptions.mangle = true
      return _args
    })
    // config.plugin('ignore').use(new webpack.ContextReplacementPlugin(/moment[/\\]locale$/, /en|de/))
  },
}
