module.exports = {
  vueFiles: './src/**/*.?(ts|vue)',
  languageFiles: './src/locales/en.json',
  output: false,
  add: true,
  remove: false,
  ci: false,
  separator: '.',
}
