// eslint-disable-next-line import/prefer-default-export
export function isIE11() {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  return !!(window as any).MSInputMethodContext && !!(document as any).documentMode
}
