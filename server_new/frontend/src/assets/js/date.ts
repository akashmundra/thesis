import moment from 'moment'
/**
 * Return most recent date as `moment(date)`
 */
// eslint-disable-next-line import/prefer-default-export
export function mostRecent(dates: Array<Date | string | number>) {
  const values = dates.map((d) => moment.utc(d).valueOf())
  return moment.utc(Math.max(...values))
}

export function getDateAxis(min: moment.MomentInput, max: moment.MomentInput) {
  const duration = moment(max).diff(min, 'months')
  let months = 1
  let format = 'YYYY MMM'
  if (duration < 6) {
    months = 1
    format = 'YYYY MMM'
  } else if (duration < 12) {
    months = 3
    format = 'YYYY MMM'
  } else if (duration < 36) {
    months = 6
    format = 'YYYY MMM'
  } else if (duration < 60) {
    months = 12
    format = 'YYYY'
  } else {
    months = 60
    format = 'YYYY'
  }
  return {
    interval: {
      min: months * 31 * 24 * 3600 * 1000,
      max: months * 31 * 24 * 3600 * 1000,
    },
    format,
    formatter: (value: number | string) => moment(value).format(format),
    durationInMonths: duration,
  }
}
