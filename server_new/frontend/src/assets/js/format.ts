import { format as __format__, formatPrefix as __formatPrefix__ } from 'd3'

const d3 = { format: __format__, formatPrefix: __formatPrefix__ }

/**
 * Formatter
 */
export interface IFormatOptions {
  asNaN?: (n: number) => boolean
  nanText?: string
  multiplier?: number
  significantDigits?: number
  specifier?: string
  showSign?: boolean
  precision?: number
  prefix?: number
  units?: string
  min?: number
  max?: number
  stripZeros?: boolean
  suffix?: string
}

const defaultFormatOptions: Required<IFormatOptions> = {
  asNaN: (_n: number) => false,
  nanText: '--',
  multiplier: 1,
  significantDigits: 3,
  specifier: 'custom',
  showSign: false,
  precision: 2,
  prefix: NaN,
  units: '',
  min: -Infinity,
  max: Infinity,
  stripZeros: false,
  suffix: '',
}

/**
 * Returns number formatter
 * @param options - format options
 */
export function format(options?: IFormatOptions) {
  // merge options with default options
  const o = { ...defaultFormatOptions, ...options }

  return (n: number): string => {
    let text = ''

    // format special values
    if (Number.isNaN(n) || o.asNaN(n)) return o.nanText
    if (n === Infinity) return '∞'
    if (n === 0) return '0'

    // multiplier
    let value = n * o.multiplier
    const min = o.min * o.multiplier
    const max = o.max * o.multiplier

    // significant digits
    if (o.significantDigits !== Infinity) {
      value = parseFloat(value.toPrecision(o.significantDigits))
    }

    // stay within boundaries
    if (value < min) {
      text = '< '
      value = min
    }
    if (value > max) {
      text = '> '
      value = max
    }

    // specifier
    let { specifier } = o
    if (o.specifier === 'custom') {
      specifier = o.showSign ? '+' : ''
      specifier += ',.'
      specifier += o.stripZeros ? '10~f' : `${o.precision}f`
    }

    // d3 prefix (e.g. 1000 for k - kilo)
    if (Number.isNaN(o.prefix)) {
      text += d3.format(specifier)(value)
    } else {
      text += d3.formatPrefix(specifier, o.prefix)(value)
    }

    // suffix
    text += o.suffix

    // units
    if (o.units) {
      text += ` ${o.units}`
    }

    return text
  }
}

/**
 * Find best options for formatter
 */

export interface IGetFormatOptions {
  significantDigits?: number
  range?: number
  units?: string
  stripZeros?: boolean
}

const defaultGetFormatOptions: Required<IGetFormatOptions> = {
  significantDigits: 3,
  range: 0,
  units: '',
  stripZeros: false,
}

export function getFormat(values: unknown[], options?: IGetFormatOptions) {
  // get options and set defaults
  const o: Required<IGetFormatOptions> = { ...defaultGetFormatOptions, ...options }

  // pre-filter values: remove duplicates and all NaN/0 values
  let _values = [...new Set(values)].filter((e) => e !== 0 && !Number.isNaN(e) && e !== undefined) as number[]
  if (_values.length === 0) _values = [1]

  // round to significant digits
  _values = _values.map((e) => parseFloat(e.toPrecision(o.significantDigits)))

  // calculate display range
  const minValue = Math.min(..._values)
  const maxValue = Math.max(..._values)

  let minOrder = Math.floor(Math.log(minValue) / Math.LN10 + Number.EPSILON)
  let maxOrder = Math.floor(Math.log(maxValue) / Math.LN10 + Number.EPSILON)

  // calculate prefix order
  if (o.range >= 0) {
    maxOrder = minOrder + o.range
    minOrder = minOrder - o.significantDigits + 1
  }
  if (o.range < 0) {
    minOrder = Math.max(maxOrder + o.range, minOrder)
    minOrder = Math.min(maxOrder - o.significantDigits + 1, minOrder)
  }
  let prefixOrder = Math.floor(minOrder / 3) * 3
  if (maxOrder - (prefixOrder + 3) > 0 && minOrder - (prefixOrder + 3) > -3) {
    prefixOrder += 3
  }
  // prefer no prefix
  if (prefixOrder === -3 && minOrder === -3) {
    prefixOrder = 0
    minOrder = -2
  } else if (prefixOrder === -3 && minOrder > -3) {
    prefixOrder = 0
  } else if (prefixOrder === -6 && minOrder > -6) {
    prefixOrder = -3
  }

  // precision
  let precision = 0
  if (minOrder < prefixOrder) {
    precision = prefixOrder - minOrder
  }

  // boundaries min/manx
  const min = o.range < 0 ? Math.min(10 ** minOrder, 10 ** prefixOrder) : -Infinity
  const max = o.range > 0 ? 10 ** maxOrder : Infinity

  // handle kg, g and t
  let multiplierOrder = 0
  if (o.units === 'kg' && prefixOrder >= 3) {
    o.units = 't'
    multiplierOrder = -3
  } else if (o.units === 'kg' && prefixOrder === -3) {
    o.units = 'g'
    multiplierOrder = 3
  } else if (o.units === 'kg' && prefixOrder === -6) {
    o.units = 'mg'
    multiplierOrder = 6
  }
  prefixOrder += multiplierOrder

  // prepare return
  const _options: IFormatOptions = {
    significantDigits: o.significantDigits,
    units: o.units,
    prefix: 10 ** prefixOrder,
    multiplier: 10 ** multiplierOrder,
    precision,
    min,
    max,
    stripZeros: o.stripZeros,
    specifier: 'custom',
  }
  return _options
}
