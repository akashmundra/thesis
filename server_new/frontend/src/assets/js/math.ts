/**
 * Return trend (precision: 2)
 */
export function trend(oldVal: number, newVal: number) {
  return Math.round((newVal / oldVal - 1) * 100) / 100
}

export type PropertyOfType<T, U> = { [K in keyof T]: T[K] extends U ? K : never }[keyof T]
/**
 * Calculates weighted average of key and volume
 */
export function weightedAvg<T extends { volume: number }>(
  objArray: T[],
  iteratee: ((value: T) => number) | PropertyOfType<T, number>,
) {
  let arr = objArray
  let iterateeSum = 0

  if (typeof iteratee === 'function') {
    arr = objArray.filter((obj) => iteratee(obj) > 0 && obj.volume > 0)
    iterateeSum = arr.map((obj) => iteratee(obj) * obj.volume).reduce((a, b) => a + b, 0)
  } else {
    arr = objArray.filter((obj) => ((obj[iteratee] as unknown) as number) > 0 && obj.volume > 0)
    iterateeSum = arr.map((obj) => ((obj[iteratee] as unknown) as number) * obj.volume).reduce((a, b) => a + b, 0)
  }

  const volumeSum = arr.map((obj) => obj.volume).reduce((a, b) => a + b, 0)

  return iterateeSum / volumeSum
}

/**
 * Round to number of decimals
 */
export function round(value: number, decimals: number) {
  return Math.round(value * 10 ** decimals) * 10 ** -decimals
}
