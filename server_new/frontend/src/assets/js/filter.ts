import _ from 'lodash'

declare interface IHeaders {
  value: string
  searchable?: boolean
}
/**
 * Return filtered data
 */
// eslint-disable-next-line import/prefer-default-export
export function filterData<T>(data: T[], headers: IHeaders[], search: string): T[] {
  const keys = headers.filter((header) => header.searchable !== false).map((obj) => obj.value)
  return search === '' || search === null
    ? data
    : data.filter((obj) => keys
      .some((key) => (_.get(obj, key) as string ? _.get(obj, key) as string
        : '').toLowerCase().includes(search.toLowerCase())))
}

export function searchObject(obj: Record<string, unknown>, search: string) {
  const object = _.omit(obj, ['id', '_id', 'url']) as Record<string, unknown>
  let result = false
  Object.values(object).forEach((element) => {
    if (element instanceof Array) {
      let found = false
      element.forEach((elem) => {
        if (typeof elem === 'string') {
          found = found || elem.toLowerCase().includes(search)
        } else {
          found = found || searchObject(elem as Record<string, unknown>, search)
        }
      })
      result = result || found
    } else if (element instanceof Object) {
      result = result || searchObject(element as Record<string, unknown>, search)
    } else if (element && (element as Record<string, unknown>).toString()) {
      result = result || (element as Record<string, unknown>).toString().toLowerCase().includes(search)
    }
  })
  return result
}

export function searchDeep<T>(data: T[], search?: string | null) {
  if (search) {
    const searchParts = search.toLowerCase().split(' ').filter((el) => !!el)
    return data.filter((obj) => searchParts.every((part) => searchObject(obj as Record<string, unknown>, part)))
  }
  return data
}
