import _ from 'lodash'

/**
 * Return ordered data
 */
// eslint-disable-next-line import/prefer-default-export
export function sortData<T>(data: T[], by: string | string[], desc: boolean | boolean[]): T[] {
  const iteratees = Array.isArray(by) ? by : [by]
  const orders = (Array.isArray(desc) ? desc : [desc]).map((el) => (el ? 'desc' : 'asc'))
  return _.orderBy(data, iteratees, orders)
}
