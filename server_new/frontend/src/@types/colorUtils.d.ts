declare type Hex = string
declare type RGBA = { r: number; g: number; b: number; a: number }

declare module 'vuetify/lib/util/colorUtils' {
  export function HexToRGBA(hex: Hex): RGBA
  export function contrastRatio(c1: RGBA, c2: RGBA): number
}
