/* eslint-disable @typescript-eslint/no-empty-interface */
import { EChartsOption } from 'echarts'

declare type UnArray<T> = T extends Array<infer U> ? U : T

declare module 'echarts' {
  // get SeriesOption$1 because echarts is not exporting
  export type SeriesOption$1 = UnArray<Required<EChartsOption>['series']>
}
