import q from '../plugins/q'

declare module 'vue/types/vue' {
  // eslint-disable-next-line no-shadow
  interface Vue {
    $q: typeof q
  }
}
