// percentile intervals
const queries = {
  low: { percentile_max: 0.2 },
  avg: {},
  high: { percentile_min: 0.8 },
}

const q = {
  queries,
}

export default q
