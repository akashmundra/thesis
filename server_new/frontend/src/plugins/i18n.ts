import Vue from 'vue'
import VueI18n from 'vue-i18n'
import moment from 'moment'
import { Store } from 'vuex'

import { configure, extend } from 'vee-validate'
import {
  required, max, min, confirmed, email,
} from 'vee-validate/dist/rules'

import enVuetify from 'vuetify/src/locale/en'
import esVuetify from 'vuetify/src/locale/es'
import zhVuetify from 'vuetify/src/locale/zh-Hans'
// import deVuetify from 'vuetify/src/locale/de'

import enValidations from 'vee-validate/dist/locale/en.json'
import esValidations from 'vee-validate/dist/locale/es.json'
import zhValidations from 'vee-validate/dist/locale/zh_CN.json'
// import deValidations from 'vee-validate/dist/locale/de.json'

import enLocale from '@/locales/en.json'
import esLocale from '@/locales/es.json'
import zhLocale from '@/locales/zh.json'
// import deLocale from '@/locales/de.json'

// import { countries as deCountriesA2 } from 'i18n-iso-countries/langs/de.json'


// const deCountries = process(deCountriesA2)

const en = {
  $vuetify: enVuetify,
  validations: enValidations,
  ...enLocale,
}
const es = {
  $vuetify: esVuetify,
  validations: esValidations,
  ...esLocale,
}
const zh = {
  $vuetify: zhVuetify,
  validations: zhValidations,
  ...zhLocale,
}
// const de = {
//   $vuetify: deVuetify,
//   validations: deValidations,
//   ...deLocale,
//   geo: { ...deCountries, ...deLocale.geo },
// }

Vue.use(VueI18n)

const i18n = new VueI18n({
  locale: 'en',
  fallbackLocale: 'en',
  messages: {
    en,
    zh,
    es,
    // de,
  },
})
export default i18n

configure({
  defaultMessage: (_field, values) => {
    // eslint-disable-next-line no-param-reassign
    values._field_ = ''
    // values._field_ = i18n.t(field) as unknown
    return String(i18n.t(`validations.messages.${values._rule_}`, values))
  },
})

extend('required', required)
extend('max', max)
extend('min', min)
extend('confirmed', confirmed)
extend('email', email)

export function setLocale(locale = 'en') {
  i18n.locale = locale
  if (locale === 'zh') {
    moment.locale('zh-cn')
  } else {
    moment.locale(locale)
  }
}

export function languageSetup(store: Store<unknown>) {
  const user = store.getters['auth/user']
  setLocale(user.language)
}
