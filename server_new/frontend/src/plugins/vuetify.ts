import Vue from 'vue'
import Vuetify from 'vuetify/lib'
import colors from 'vuetify/lib/util/colors'
import i18n from './i18n'

Vue.use(Vuetify)

export default new Vuetify({
  theme: {
    options: {
      customProperties: true,
    },
    themes: {
      light: {
        primary: '#1B4158',
        secondary: '#00C4B9',
        accent: '#82B1FF',
        error: '#CD2940',
        info: '#2196F3',
        success: '#4CAF50',
        warning: '#E47F35',
        // default color coding (seller view - high green, low red) (changed in AppBar.vue)
        high: '#00e5be', // secondary
        low: '#CD2940', // error
        neutral: colors.grey.base,
      },
      dark: {
        primary: '#1B4158',
        secondary: '#ff0000',
        accent: '#82B1FF',
        error: '#CD2940',
        info: '#2196F3',
        success: '#4CAF50',
        warning: '#E47F35',
        // default color coding (seller view - high green, low red) (changed in AppBar.vue)
        high: '#00e5be', // secondary
        low: '#CD2940', // error
        neutral: colors.grey.base,
      },
    },
  },
  // internationalization (i18n)
  lang: {
    t: (key, ...params) => i18n.t(key, params) as string,
  },
})
