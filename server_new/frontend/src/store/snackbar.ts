import Vue from 'vue'
import Vuex, { Module } from 'vuex'

Vue.use(Vuex)

export interface ISnackbarState {
  message: string
  color: string
  timeout: number
}

export const snackbar: Module<ISnackbarState, ISnackbarState> = {
  namespaced: true,
  state: {
    message: '',
    color: '',
    timeout: 5000,
  },
  mutations: {
    SET_MESSAGE(state, payload) {
      state.message = payload.message
      state.color = payload.color
      state.timeout = payload.timeout
    },
  },
  actions: {
    showMessage({ commit }, snackbarContent) {
      commit('SET_MESSAGE', snackbarContent)
    },
  },
}
