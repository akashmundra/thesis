/* eslint-disable import/prefer-default-export */
import Vue from 'vue'
import Component from 'vue-class-component'

import * as d3 from 'd3'

declare interface ISVG {
  width: number
  height: number
  g: d3.Selection<d3.BaseType, unknown, null, undefined>
}

declare interface IScale {
  isVisible: boolean
  scale: d3.ScaleBand<string>
  axis: d3.Axis<string>
  init: () => void
  update: (domain?: string[]) => void
}

@Component
export class ScaleBandBottom extends Vue {
  public svg = {} as ISVG

  public data!: { x: string }[][]

  public bottomAxis = {} as IScale

  public topAxis = {} as IScale

  public created() {
    /**
     * Bottom
     */
    this.bottomAxis.init = () => {
      this.bottomAxis.isVisible = true
      this.bottomAxis.scale = d3.scaleBand().range([0, this.svg.width]).paddingInner(0.1)
      this.bottomAxis.axis = d3.axisBottom(this.bottomAxis.scale).tickSizeOuter(0).tickSizeInner(0).tickPadding(14)

      this.svg.g.append('g').attr('class', 'axis bottom').attr('transform', `translate(0, ${this.svg.height})`)
    }

    this.bottomAxis.update = (domain?: string[]) => {
      this.bottomAxis.scale.domain(domain || [...new Set(this.data.flat().map((d) => d.x))])

      if (this.bottomAxis.isVisible) {
        this.svg.g.select<SVGSVGElement>('.axis.bottom').transition().call(this.bottomAxis.axis)
      }
    }

    /**
     * Top
     */
    this.topAxis.init = () => {
      this.topAxis.isVisible = true
      this.topAxis.scale = this.bottomAxis.scale
      this.topAxis.axis = d3.axisTop(this.topAxis.scale).tickValues([]).tickSizeOuter(0)

      this.svg.g.append('g').attr('class', 'axis top')
    }

    this.topAxis.update = () => {
      if (this.topAxis.isVisible) {
        this.svg.g.select<SVGSVGElement>('.axis.top').transition().call(this.topAxis.axis)
      }
    }
  }
}
