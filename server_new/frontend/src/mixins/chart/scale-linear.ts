/* eslint-disable max-classes-per-file */
import Vue from 'vue'
import Component from 'vue-class-component'

import * as d3 from 'd3'

declare interface ISVG {
  width: number
  height: number
  g: d3.Selection<d3.BaseType, unknown, null, undefined>
}

declare interface IScale {
  isVisible: boolean
  scale: d3.ScaleLinear<number, number>
  axis: d3.Axis<number | { valueOf(): number }>
  init: () => void
  update: (domain?: [number, number]) => void
}

function getDomain(values: number[]) {
  const extend = d3.extent(values)
  let domain = [extend[0] || 0, extend[1] || 0]
  domain = [domain[0] * 0.95, domain[1] * 1.05]
  return domain
}

@Component
export class ScaleLinearBottom extends Vue {
  public svg = {} as ISVG

  public data!: { x: number }[][]

  public bottomAxis = {} as IScale

  public topAxis = {} as IScale

  public created() {
    /**
     * Bottom
     */
    this.bottomAxis.init = () => {
      this.bottomAxis.isVisible = true
      this.bottomAxis.scale = d3.scaleLinear().range([0, this.svg.width])
      this.bottomAxis.axis = d3
        .axisBottom(this.bottomAxis.scale)
        .ticks(4)
        .tickSizeOuter(0)
        .tickSizeInner(0)
        .tickPadding(14)

      this.svg.g.append('g').attr('class', 'axis bottom').attr('transform', `translate(0, ${this.svg.height})`)
    }

    this.bottomAxis.update = (domain?: [number, number]) => {
      const _domain = domain || getDomain(this.data.flat().map((d) => d.x))
      this.bottomAxis.scale.domain(_domain)

      if (this.bottomAxis.isVisible) {
        this.svg.g.select<SVGSVGElement>('.axis.bottom').transition().call(this.bottomAxis.axis)
      }
    }

    /**
     * Top
     */
    this.topAxis.init = () => {
      this.topAxis.isVisible = true
      this.topAxis.scale = this.bottomAxis.scale
      this.topAxis.axis = d3.axisTop(this.bottomAxis.scale).ticks(0).tickSizeOuter(0)

      this.svg.g.append('g').attr('class', 'axis top')

      this.topAxis.update = () => {
        if (this.topAxis.isVisible) {
          this.svg.g.select<SVGSVGElement>('.axis.top').transition().call(this.topAxis.axis)
        }
      }
    }
  }
}

@Component
export class ScaleLinearLeft extends Vue {
  public svg = {} as ISVG

  public data!: { y: number; y0?: number }[][]

  public leftAxis = {} as IScale

  public rightAxis = {} as IScale

  public created() {
    /**
     * Left
     */
    this.leftAxis.init = () => {
      this.leftAxis.isVisible = true
      this.leftAxis.scale = d3.scaleLinear().range([this.svg.height, 0])
      this.leftAxis.axis = d3.axisLeft(this.leftAxis.scale).ticks(4).tickSizeOuter(0).tickSizeInner(0)
        .tickPadding(14)

      this.svg.g.append('g').attr('class', 'axis left')
    }

    this.leftAxis.update = (domain?: [number, number]) => {
      const _domain = domain
      || getDomain(
        this.data
          .flat()
          .flatMap((d) => [d.y, d.y0])
          .filter((d) => d !== undefined) as number[],
      )
      this.leftAxis.scale.domain(_domain)

      if (this.leftAxis.isVisible) {
        this.svg.g.select<SVGSVGElement>('.axis.left').transition().call(this.leftAxis.axis)
      }
    }

    /**
     * Right
     */
    this.rightAxis.init = () => {
      this.rightAxis.axis = d3.axisRight(this.leftAxis.scale).ticks(0).tickSizeOuter(0)

      this.svg.g.append('g').attr('class', 'axis right').attr('transform', `translate(${this.svg.width}, 0)`)

      this.rightAxis.update = () => {
        this.svg.g.select<SVGSVGElement>('.axis.right').transition().call(this.rightAxis.axis)
      }
    }
  }
}
