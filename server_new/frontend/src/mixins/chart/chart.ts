/* eslint-disable import/prefer-default-export */
// @ts-nocheck
// should become echarts so we ignore type errors for now
import Vue from 'vue'
import Component from 'vue-class-component'

import * as d3 from 'd3'

declare interface IOptions {
  width?: number
  height?: number
  aspectRatio?: number
  margin?: {
    top: number
    right: number
    bottom: number
    left: number
  }
}

const defaultOptions: Required<IOptions> = {
  width: 0,
  height: 0,
  aspectRatio: 1.78, // 16/9
  margin: {
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
  },
}

declare interface ISVG {
  wrapper: HTMLElement
  el: d3.Selection<SVGElement, unknown, null, undefined>
  g: d3.Selection<d3.BaseType, unknown, null, undefined>
  width: number
  height: number
  options: Required<IOptions>
  init: (wrapper: HTMLElement, options?: IOptions) => void
}

@Component
export class Chart extends Vue {
  public svg = {} as ISVG

  public created() {
    this.svg.init = (wrapper: HTMLElement, options?: IOptions) => {
      this.svg.wrapper = wrapper

      d3.select(wrapper).selectAll('svg').remove()

      this.svg.el = d3.select(wrapper).append<SVGElement>('svg').attr('preserveAspectRatio', 'xMidYMid meet')

      this.svg.options = { ...defaultOptions, ...options }

      const width = this.svg.options.width || this.svg.wrapper.clientWidth
      const height = this.svg.options.height || Math.floor(width / this.svg.options.aspectRatio)

      this.svg.el.attr('width', width).attr('height', height).attr('viewBox', `0 0 ${width} ${height}`)

      this.svg.g = this.svg.el
        .append('g')
        .attr('transform', `translate(${this.svg.options.margin.left}, ${this.svg.options.margin.top})`)

      // in d3, width and height exclude the margins
      this.svg.width = width - this.svg.options.margin.left - this.svg.options.margin.right
      this.svg.height = height - this.svg.options.margin.top - this.svg.options.margin.bottom
    }
  }
}
