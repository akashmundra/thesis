import Vue from 'vue'
import VueRouter from 'vue-router'

import Tool from '@/views/Tool.vue'

Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'tool',
      components: {
        default: Tool,
      },
    },
  ],
})

export default router
