export type conditionType = '>' | '<'
export type metricType = 
'cpu.Branch Misprediction Rate' |
'cpu.Branch Misprediction Ratio' |
'cpu.Branch Rate' |
'cpu.CPI' | 
'cpu.Clock' | 
'cpu.FLOPS DP' |
'cpu.FLOPS SP' |
'cpu.Instructions per Branch' |
'cpu.Load to Store Ratio' |
'cpu.Packed' |
'cpu.Scalar' |
'cpu.Uncore Clock' |
'cpu.Usage Iowait' |
'cpu.Usage Nice' |
'cpu.Usage System' |
'cpu.Usage Total' |
'cpu.Usage User' |
'cpu.Usage Virtual' |
'cpu.Vectorization Ratio' |
'cycles.Cycles without execution due to L1D' |
'cycles.Cycles without execution due to L2' |
'cycles.Cycles without execution due to memory loads' |
'cycles.Execution stall rate' |
'cycles.Stalls caused by L1D misses rate' |
'cycles.Stalls caused by L2 misses' |
'cycles.Stalls caused by L2 misses rate' |
'cycles.Stalls caused by memory loads' |
'cycles.Stalls caused by memory loads rate' |
'cycles.Total execution stalls' |
'energy.CPU Energy' |
'energy.CPU Power' |
'energy.power' |
'io.Average Read Request Size' |
'io.Average Read Wait Time' |
'io.Average Request Queue Length' |
'io.Average Write Request Size' |
'io.Average Write Wait Time' |
'io.Merged Read Requests' |
'io.Read KB/s' |
'io.Read Requests/s' |
'io.Utilization' |
'io.Write KB/s' |
'io.Write Requests/s' |
'mem.Active' |
'mem.Buffers' |
'mem.Cached' |
'mem.Inactive' |
'mem.L2 Bandwidth' |
'mem.L2 Data Volume' |
'mem.L2 Miss Rate' |
'mem.L2 Miss Ratio' |
'mem.L2 Request Rate' |
'mem.L2D Evict Bandwidth' |
'mem.L2D Evict Data Volume' |
'mem.L2D Load Bandwidth' |
'mem.L2D Load Data Volume' |
'mem.L3 Bandwidth' |
'mem.L3 Data Volume' |
'mem.L3 Miss Rate' |
'mem.L3 Miss Ratio' |
'mem.L3 Request Rate' |
'mem.MemAvailable' |
'mem.MemFree' |
'mem.MemTotal' |
'mem.MemUsage' |
'mem.MemUsed' |
'mem.Memory Data Volume' |
'mem.Memory Read Bandwidth' |
'mem.Memory Read Volume' |
'mem.Memory Write Bandwidth' |
'mem.Memory Write Volume' |
'mem.Memory bandwidth' |
'mem.SwapCached' |
'mem.SwapFree' |
'mem.SwapTotal' |
'roofline.AVX FLOPS DP' |
'roofline.AVX FLOPS SP' |
'roofline.Operational Intensity'

export type metricDomain = 'cpu' | 'mem' | 'io' | 'energy' | 'cycles' | 'roofline'
