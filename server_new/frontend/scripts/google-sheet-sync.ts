// eslint-disable-next-line import/no-extraneous-dependencies
import { google } from 'googleapis'

// get service account credentials
import credentials from './credentials.json'

const CLIENT_EMAIL = credentials.client_email
const PRIVATE_KEY = credentials.private_key
const SCOPES = ['https://www.googleapis.com/auth/spreadsheets']

const auth = new google.auth.JWT(CLIENT_EMAIL, undefined, PRIVATE_KEY, SCOPES)
const sheets = google.sheets({ version: 'v4', auth })

interface ISheetDataOptions {
  spreadsheetId: string
  range: string
  values?: unknown[][]
}

export async function getSheetData(params: ISheetDataOptions) {
  const res = await sheets.spreadsheets.values.get(params)
  return res.data.values || []
}

export async function sendDataToSheet(params: ISheetDataOptions) {
  await sheets.spreadsheets.values.update({
    spreadsheetId: params.spreadsheetId,
    range: params.range,
    valueInputOption: 'RAW',
    requestBody: { values: params.values },
  })
}

export function sheet2records(values: unknown[][]) {
  const headers = (values.shift() || []).map((e) => String(e))
  return values.map((row) => Object.assign({}, ...headers.map((header, i) => ({ [header]: row[i] }))))
}

// export function record2Sheet<T extends string>(arr: Array<Record<T, unknown>>, headers: Array<T>): unknown[][] {
//   return arr.map((obj) => headers.map((header) => obj[header]))
// }
