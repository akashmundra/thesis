/* eslint-disable import/no-extraneous-dependencies */
import { flatten, unflatten } from 'flat'
import path from 'path'
import fs from 'fs'

import en from '../src/locales/en.json'

const enFlat: Record<string, string> = flatten(en)

const sortedEnFlat: Record<string, string> = {}

Object.keys(enFlat).sort((a, b) => a.toLowerCase()
  .localeCompare(b.toLowerCase())).forEach((obj) => { sortedEnFlat[obj] = enFlat[obj] })
const fileContent = JSON.stringify(unflatten(sortedEnFlat), null, 4)
fs.writeFileSync(path.resolve(__dirname, '../src/locales/en.json'), fileContent)
