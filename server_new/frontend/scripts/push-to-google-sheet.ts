/* eslint-disable import/no-extraneous-dependencies */
import { flatten } from 'flat'

import { getSheetData, sendDataToSheet } from './google-sheet-sync'
import { languages, spreadsheetId } from './config'

import en from '../src/locales/en.json'

const enFlat: Record<string, string> = flatten(en)

const headers = ['key', 'en', 'translation'] as const
declare type Row = Record<typeof headers[number], string>

export async function updateSheet(language: string) {
  const range = language === 'en' ? 'en!A:B' : `${language}!A:C`
  let data = await getSheetData({ spreadsheetId, range })

  const sheetKeys = data.map((row) => String(row[0]))
  const enKeys = Object.keys(enFlat)

  const newKeys = enKeys.filter((key) => !sheetKeys.includes(key))
  const newData = language === 'en'
    ? newKeys.map((key) => [key, enFlat[key]])
    : newKeys.map((key) => [key, enFlat[key], 'TRANSLATE'])

  data = [...data, ...newData]
  await sendDataToSheet({ spreadsheetId, range, values: data })
}

export async function main() {
  languages.forEach(async (language) => {
    await updateSheet(language)
  })
}

main()
  .then(() => console.info('Translation push successful'))
  .catch((err) => console.error(err))
