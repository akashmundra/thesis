/* eslint-disable import/no-extraneous-dependencies */
import { flatten } from 'flat'
import { getSheetData, sendDataToSheet } from './google-sheet-sync'
import * as config from './config'

// from en.json
import translationKeys from '../src/locales/en_test.json'

export async function createJSON() {
  const { spreadsheetId } = config
  const dictKeys: { [key: string]: string } = flatten(translationKeys)

  const keyList = Object.keys(dictKeys)

  // from google sheets
  const esp: Array<{ key: string; en: string; es: string }> = await getSheetData({
    spreadsheetId,
    range: 'test_es!A:C',
  })

  const zh: Array<{ key: string; en: string; zh: string }> = await getSheetData({
    spreadsheetId,
    range: 'test_zh!A:C',
  })
  
  const esDictionary: { [key: string]: { en: string; es: string } } = {}
  const zhDictionary: { [key: string]: { en: string; zh: string } } = {}

  for (let i = 0; i < esp.length; i += 1) {
    esDictionary[esp[i].key] = { en: esp[i].en, es: esp[i].es }
  }

  for (let i = 0; i < zh.length; i += 1) {
    zhDictionary[zh[i].key] = { en: zh[i].en, zh: zh[i].zh }
  }

  const rowEng = [['key', 'en']]
  const rowEsp = [['key', 'en', 'es']]
  const rowZh = [['key', 'en', 'zh']]

  for (let i = 0; i < keyList.length; i += 1) {
    rowEng.push([keyList[i], dictKeys[keyList[i]]])

    // spanish
    if (keyList[i] in esDictionary) {
      rowEsp.push([keyList[i], esDictionary[keyList[i]].en, esDictionary[keyList[i]].es])
    } else {
      rowEsp.push([keyList[i], dictKeys[keyList[i]], '-'])
    }

    // chinese
    if (keyList[i] in zhDictionary) {
      rowZh.push([keyList[i], zhDictionary[keyList[i]].en, zhDictionary[keyList[i]].zh])
    } else {
      rowZh.push([keyList[i], dictKeys[keyList[i]], '-'])
    }
  }

  console.log(rowEng)

  sendDataToSheet({
    spreadsheetId,
    range: 'test_en!A:B',
    value: rowEng,
  })
  
  sendDataToSheet({
    spreadsheetId,
    range: 'test_es!A:C',
    value: rowEsp,
  })

  sendDataToSheet({
    spreadsheetId,
    range: 'test_zh!A:C',
    value: rowZh,
  })
}

export async function main() {
  await createJSON()
}

main().catch((err) => console.error(err))
