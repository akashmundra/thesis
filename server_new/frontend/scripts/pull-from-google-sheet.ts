/* eslint-disable import/no-extraneous-dependencies */
import path from 'path'
import fs from 'fs'
import { unflatten } from 'flat'
import _ from 'lodash'

import { getSheetData, sheet2records } from './google-sheet-sync'
import { languages, spreadsheetId } from './config'

const headers = ['key', ...languages] as const
declare type Row = Record<typeof headers[number], string>

export default async function createJSONFiles() {
  // get sheet data
  const data = await getSheetData({
    spreadsheetId,
    range: `download!A:${String.fromCharCode(65 + headers.length)}`, // 65 is 'A'
  })
  let records: Row[] = sheet2records(data)
  records = _.sortBy(records, (row) => row.key.toLowerCase())

  // check integrity
  const conflicts: string[] = []
  const keys = records.map((row) => row.key)
  keys.forEach((key1) => {
    keys.forEach((key2) => {
      const text1 = `^${key1}`
      const text2 = `^${key2}.`
      if (text1.includes(text2)) {
        conflicts.push(text2)
      }
    })
  })
  if (conflicts.length) {
    console.error('Conflicts detected:')
    conflicts.forEach((key) => {
      console.error(key)
    })
    throw Error('Key nesting error!')
  }
  console.info(`Pulled translations: ${records.length}`)

  // build json files for each language and save to .json
  languages.forEach((language) => {
    const dict = Object.assign({}, ...records.map((row) => ({ [row.key]: row[language] })))
    const fileContent = JSON.stringify(unflatten(dict), null, 4)
    fs.writeFileSync(path.resolve(__dirname, `../src/locales/${language}.json`), fileContent)
  })
}

createJSONFiles()
  .then(() => console.info('Translation pull successful'))
  .catch((err) => console.error(err))
