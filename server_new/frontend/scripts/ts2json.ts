import path from 'path'
import fs from 'fs'

import en from '../src/locales/en'

fs.writeFileSync(path.resolve(__dirname, '../src/locales/en.json'), JSON.stringify(en))
