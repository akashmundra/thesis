## v0.4.0 (2020-04-05)

### Fixed (1 changes)
- Bug in color coding

### Added (4 changes)
- Company portfolio
- Trader/manufacturer selector
- Company hierarchy (database migration)
- Substance hierarchy (database migration)

### Changed (0 changes)

### Removed (0 changes)

### Security (0 changes)

### Performance (2 changes)
- Flask caching (quick fix)
- Flask replicas

### Other (0 changes)

## v0.3.1 (2020-03-15)

### Fixed (3 changes)
- Welcome screen spark-lines not sorting
- Double page load trigger in filter sidebar
- API error on `date_max` not set

### Added (2 changes)
- Loading indicators
- Filter sidebar
  - Pharmacopeia: Regulated (BP, EP USP, IP, JP), Unregulated
  - Regulated markets
  - Volume selector

### Changed (1 changes)
- Company profile model cleanup

### Removed (1 changes)
- Legacy tests

### Security (0 changes)

### Performance (0 changes)

### Other (0 changes)


## v0.3.0 (2020-03-01)

### Fixed (8 changes)
- Removed country '---' from KPIS
- Portfolio match does no longer interfere with company update
- Formatting issue in price by origin/destination fixed
- Page view not starting at the top of the page
- Supplier/buyer list resets data on url change
- Chart and map hover works reliably now
- Password change API working again
- Mobile keyboard now hides on substance select

### Added (4 changes)
- Portfolio match in Supplier/Buyer list based on subscriptions
- Custom color coding for supplier und buyer
- Limited/No data banner added to benchmark page
- New version of Market dynamics with reference price/volume and spend

### Changed (3 changes)
- Company profile text changes
- Chart hover is now more verbose (lists x and y values)
- Login email is case insensitive

### Removed (0 changes)

### Security (2 changes)
- Removed source map from production build
- Mangle class and function names to obfuscate code

### Performance (1 changes)
- Docker build cluster auto-scales based on cpu and ram requests

### Other (0 changes)
